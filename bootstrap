eval '(exit $?0)' && eval 'exec perl -wS "$0" "$@"'
  & eval 'exec perl -wS "$0" $argv:q'
    if 0;

use strict;
use warnings;
use File::Basename;
use File::Path qw(make_path);
use Getopt::Long qw(:config gnu_getopt no_ignore_case);
use Sys::Hostname;
use POSIX qw(strftime);
use Pod::Usage;
use Pod::Man;
use Cwd 'abs_path';

=head1 NAME

bootstrap - creates a framework for writing a Varnish module

=head1 SYNOPSIS

B<bootstrap>
[B<-f>]
[B<-C> I<DIR>]
[B<--license=>I<FILE>]
[B<--description=>I<TEXT>]    
[B<--directory=>I<DIR>]
[B<--email=>I<EMAIL>]
[B<--force>]
[B<--foreign>]
[B<--git>]
[B<--gnits>]
[B<--real-name=>I<NAME>]
[B<--url=>I<URL>]    
[B<--version=>I<V>]
I<MODULE>

=head1 DESCRIPTION

The B<bootstrap> tool populates the current working directory (or the
directory specified with the B<-C> option) with the files necessary
for building Varnish module B<vmod_I<MODULE>> using GNU autotools.
At the end of the run, it invokes B<autoreconf>(1) to actually bootstrap
the suite.    
    
=head1 OPTIONS

=over 4

=item B<-f>, B<--force>

Force overwriting existing files.

=item B<-C>, B<--directory=>I<DIR>

Change to I<DIR> prior to startup.

=item B<--git>

Set Automake strictness to B<gnu>.    
    
=item B<--gnits>

Set Automake strictness to B<gnits>.    

=item B<--foreign>

Set Automake strictness to B<foreign>. This is the default.

=item B<--real-name=>I<NAME>

Sets your real name.

=item B<--email=>I<EMAIL>

Sets your email address.

=item B<--description=>I<TEXT>

Sets a one-line vmod description for use in the vcc file header.

=item B<--version=>I<V>

Sets the vmod version. Default is B<0.1>.

=item B<--url=>I<URL>

Sets the ULR of the vmod web page.    

=item B<--license=>I<FILE>

Reads the text of the license from I<FILE>. Unless I<FILE> begins with B</> or
B<./>, the file will first be looked in the B<license> subdirectory and then
in the current working directory.

The text read froom I<FILE> will be expanded (see B<VARIABLE EXPANSION>)
and reproduced in the initial header of each created file.
    
=item B<-?>

Display short help.

=item B<--help>

Display detailed help.    

=item B<--usage>

Display short usage summary.    
    
=back    
    
=head1 SEE ALSO

The code.    
    
=cut    

my $program = basename($0);
my $progdescr = q{creates a framework for writing a Varnish module};
my $basedir = abs_path(dirname($0));
my $tmpldir = $basedir . '/tmpl';
my $force;
my $git = 0;
my %dict = ( STRICTNESS => 'foreign',
	     VERSION => '0.1',
	     DATE => strftime('%Y-%m-%d', localtime),
	     YEAR => strftime('%Y', localtime),
	     DESCR => 'No description yet',
	     URL => q{<please enter the project's URL here>},
    );

sub error {
    print STDERR "$program: @_\n";
}
    
sub usage {
    pod2usage(-exitstatus => 0, -verbose => 0);
}

sub create_dir {
    my $dir = shift;
    make_path($dir, {error => \my $err});
    if (@$err) {
	for my $diag (@$err) {
	    my ($file, $message) = %$diag;
	    if ($file eq '') {
		print STDERR $message . ", while creating \"$file\"\n";
	    } else {
		print STDERR $message . ", while creating \"$file\"\n";
	    }
	}
	exit(1);
    }
}

sub expvar {
    my ($varname, $args) = @_;
    my $exp;
    $args = {} unless $args;
    if (exists($dict{$varname})) {
	if (ref($dict{$varname}) eq 'SCALAR') {
	    $exp = ${$dict{$varname}};
	} elsif (ref($dict{$varname}) eq 'CODE') {
	    $exp = &{$dict{$varname}}($varname, $args);
	} else {
	    $exp = $dict{$varname};
	}
    } elsif (exists($args->{$varname})) {
	$exp = $args->{$varname};
    } else {
	return "";
    }
    if ($exp ne '' && exists($args->{prefix})) {
	my @lines = split /\n/, $exp;
	if ($args->{suffix}) {
	    my $leng = 0;
	    foreach my $l (@lines) {
		$leng = length($l) if length($l) > $leng;
	    }
	    $exp = join("\n",
			map {
			    $args->{prefix} . $_ . ' ' x ($leng - length($_)) . $args->{suffix}
			} @lines);
	} else {
	    $exp = join("\n", map { $args->{prefix} . $_ } @lines);
	}
    }
    return $exp;
}   

sub make_file {
    my ($name, $args) = @_;
    my $template = $tmpldir . '/' . $args->{template};
    open(my $ofd, '>', $name)
	or die "can't open $name for writing: $!";
    open(my $ifd, '<', $template)
	or die "can't open $template: $!";
    while (<$ifd>) {
	s/^(.*)%\[([\w_]+)\](.*)$
         /expvar($2, { %$args, prefix => $1, suffix => $3 })
         /ex;
	s/\%\{([\w_]+)\}/expvar($1, $args)/gex;
	print $ofd $_;
    }
    close $ifd;
    close $ofd;
}

sub mkcl {
    my ($name) = @_;
    open(my $fd, '>', $name)
	or die "can't open $name for writing: $!";
    print $fd <<'EOT'
This file is a placeholder. It will be replaced with the actual ChangeLog
by make dist.  Run make ChangeLog if you wish to create it earlier.
EOT
;
    close $fd;
}

sub mkCOPYING {
    my ($name, $args) = @_;
    my $template = $basedir . '/COPYING';
    return if -d '.git';
    open(my $ofd, '>', $name)
	or die "can't open $name for writing: $!";
    open(my $ifd, '<', $template)
	or die "can't open $template: $!";
    while (<$ifd>) {
	s/\%\{([\w_]+)\}/expvar($1, $args)/gex;
	print $ofd $_;
    }
    close $ifd;
    close $ofd;
}

sub strictness_gnu {
    return $dict{STRICTNESS} eq 'gnu' || $dict{STRICTNESS} eq 'gnits';
}

sub strictness_gnits {
    return $dict{STRICTNESS} eq 'gnits';
}

my @files = (
    { name => 'Makefile.am', template => 'Makefile.am' },
    { name => 'configure.ac', template => 'configure.ac' },
    { name => '.gitignore', template => 'dot.gitignore',
      cond => \$git },
    { name => 'src/Makefile.am', template => 'src/Makefile.am' },
    { name => 'src/.gitignore', template => 'src/dot.gitignore',
      cond => \$git },
    { name => 'src/vmod_%{MODULE}.vcc', template => 'src/vmod.vcc' },
    { name => 'src/vmod_%{MODULE}.c', template => 'src/vmod.c' },
    { name => 'tests/testsuite.at', template => 'tests/testsuite.at' },
    { name => 'tests/atlocal.in', template => 'tests/atlocal.in' },
    { name => 'tests/.gitignore', template => 'tests/dot.gitignore',
      cond => \$git },
    { name => 'tests/Makefile.am', template => 'tests/Makefile.am' },
    { name => 'COPYING', func => \&mkCOPYING },
    { name => 'AUTHORS', template => 'AUTHORS',
      cond => \&strictness_gnu },
    { name => 'NEWS', template => 'NEWS',
      cond => \&strictness_gnu },
    { name => 'README', template => 'README',
      cond => \&strictness_gnu },
    { name => 'THANKS', template => 'THANKS',
      cond => \&strictness_gnits },
    { name => 'ChangeLog', func => \&mkcl,
      cond => \&strictness_gnu, gitignore => 1 },
    { name => 'm4', func => \&create_dir }
);


my $wd;
my $license_file;
GetOptions(
    'h|?' => sub {
	pod2usage(-message => "$program: $progdescr",
		  -exitstatus => 0)
    },
    'help' => sub {
	pod2usage(-exitstatus => 0,
		  -verbose => 2);
    },
    'usage' => sub {
	pod2usage(-exitstatus => 0,
                  -verbose => 0);
    },
    
    'force|f' => \$force,
    'firectory|C=s' => \$wd,
    'git' => \$git,
    'gnu' => sub { $dict{STRICTNESS} = 'gnu' },
    'gnits' => sub { $dict{STRICTNESS} = 'gnits' },
    'foreign' => sub { $dict{STRICTNESS} = 'foreign' },
    'real-name=s' => \$dict{REALNAME},
    'email=s' => \$dict{EMAIL},
    'description=s' => \$dict{DESCR},
    'version=s' => \$dict{VERSION},
    'url=s' => \$dict{URL},
    'license=s' => \$license_file
    ) or exit(1);

$dict{MODULE} = shift @ARGV or usage;

usage if @ARGV;

$dict{EMAIL} = $ENV{USER} . '@' . hostname()
    unless defined($dict{EMAIL});
unless (defined($dict{REALNAME})) {
    $dict{REALNAME} = (getpwnam($ENV{USER}))[6];
    $dict{REALNAME} =~ s/,.*//;
    $dict{REALNAME} = "User $ENV{USER}" if $dict{REALNAME} eq '';
}

if ($license_file) {
    unless ($license_file =~ m{^(\.{1,2})?/}) {
	my $n = "$basedir/license/$license_file";
	$license_file = $n if -f $n;
    }

    local $/ = undef;
    open(my $fd, '<', $license_file)
	or die "cannot open $license_file: $!";
    $dict{LICENSE} = <$fd>;
    close($fd);
    $dict{LICENSE} =~ s/\%\{([\w_]+)\}/expvar($1)/gex;
}

if ($wd) {
    create_dir($wd) unless -d $wd;
    system("cp -r $basedir $wd");
    chdir $wd or die "Can't change to $wd: $!";
}

$git = 1 if (!$git && -d '.git');

my $skip = 0;
my @created;
foreach my $f (@files) {
    my $func;
    my $name;

    if (exists($f->{cond})) {
	my $cond;
	if (ref($f->{cond}) eq 'CODE') {
	    $cond = &{$f->{cond}}($f);
	} elsif (ref($f->{cond}) eq 'SCALAR') {
	    $cond = ${$f->{cond}};
	} else {
	    die "$f->{name}: invalid condition";
	}
	next unless $cond;
    }
    
    $name = $f->{name};
    $name =~ s/\%\{([\w_]+)\}/expvar($1)/gex;

    if (-e $name) {
	unless ($force) {
	    error "$name already exists, skipping";
	    $skip++;
	}
    }
    
    my $dir = dirname($name);
    create_dir($dir) unless -d $dir;
    
    if (exists($f->{func})) {
	$func = $f->{func};
    } elsif (exists($f->{template})) {
	$func = \&make_file;
    } else {
	die "neither func nor template set for $f->{name}";
    }
    &{$func}($name, $f);

    push @created, $name if -f $name && !$f->{gitignore};
}

if ($skip) {
    error "use '$program --force' to overwrite existing files";
}

system('autoreconf -f -i -s');

if ($git) {
    system("git init") unless -d '.git';
    system("git submodule add git://git.gnu.org.ua/acvmod.git");
    system('git', 'add', @created);
}

# Local variables:
# mode: perl
# End:
